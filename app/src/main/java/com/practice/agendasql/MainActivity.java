package com.practice.agendasql;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import database.AgendaContactos;
import database.AgendaDbHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {
    private TextView txtNombre;
    private TextView txtTel1;
    private TextView txtTel2;
    private TextView txtDireccion;
    private TextView txtNotas;
    private CheckBox chkFavorito;
    private Button btnLimpiar;
    private Button btnGuardar;
    private Button btnListar;
    private AgendaContactos db;
    private long id;
    private Contacto savedContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtTel1 = (TextView) findViewById(R.id.txtTel1);
        txtTel2 = (TextView) findViewById(R.id.txtTel2);
        txtDireccion = (TextView) findViewById(R.id.txtDireccon);
        txtNotas = (TextView) findViewById(R.id.txtNotas);
        chkFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        db = new AgendaContactos(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().equals("") || txtTel1.getText().toString().equals("")
                || txtDireccion.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"Ingrese Nombre, Telefono y Direccion",Toast.LENGTH_SHORT).show();
                }else {
                    Contacto c = new Contacto();
                    c.setNombre(txtNombre.getText().toString());
                    c.setTelefono1(txtTel1.getText().toString());
                    c.setTelefono2(txtTel2.getText().toString());
                    c.setDomicilio(txtDireccion.getText().toString());
                    c.setNotas(txtNotas.getText().toString());
                    if(chkFavorito.isChecked()){
                        c.setFavorito(1);
                    }else{
                        c.setFavorito(0);
                    }
                    db.openDatabase();
                    if(savedContact == null){
                        long idx = db.insertContacto(c);
                        Toast.makeText(MainActivity.this,"Se agregó contacto con ID: " + idx,Toast.LENGTH_SHORT).show();
                    }else{
                        db.updateContacto(c,id);
                        Toast.makeText(MainActivity.this,"Se actualizó el registro: " + id,Toast.LENGTH_SHORT).show();
                    }
                    db.cerrar();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,listActivity.class);
                startActivityForResult(intent,0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(Activity.RESULT_OK == resultCode){
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = contacto.getID();
            txtNombre.setText(contacto.getNombre());
            txtTel1.setText(contacto.getTelefono1());
            txtTel1.setText(contacto.getTelefono2());
            txtDireccion.setText(contacto.getDomicilio());
            txtNotas.setText(contacto.getNotas());
            if(contacto.getFavorito() > 0){
                chkFavorito.setChecked(true);
            }
        }else{
            limpiar();
        }
    }

    private void limpiar(){
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtDireccion.setText("");
        txtNotas.setText("");
        chkFavorito.setChecked(false);
    }
}
